#include "ascii_framebuffer.hpp"

#include <vector>
#include <cmath>
#include <random>

using namespace ascii_framebuffer;

const std::vector<std::string> knight =
{
  "   __,  ",  /*   __,  */
  " /  o\\  ", /* /  o\  */
  " \\  \\_> ",/* \  \_> */
  " /__\\   "  /* /__\   */
};
constexpr char star_char = '.';

struct Vec3
{
  float x,y,z;
};

Vec3 perspective(const Vec3& position, const float vertical_field_of_view, const float horizontal_field_of_view)
{
  const float B_y = std::tan(vertical_field_of_view)*position.z;
  const float B_x = std::tan(horizontal_field_of_view)*position.z;
  Vec3 ret = {0.0f,0.0f,position.z};
  ret.x = position.x/B_x;
  ret.y = position.y/B_y;
  return ret;
}

int main()
{

  Framebuffer fb;
  constexpr float vertical_field_of_view = 1.1;// radians
  const float horizontal_field_of_view = vertical_field_of_view*((float)fb.width()/(float)fb.height());
  constexpr float factor = 19000.0;
  constexpr Vec3 velocity{0.0, 0.0, -8000.0};
  constexpr float start_pos_y = 18000.0;
  const float spawn_interval = 0.000002 * fb.height()*fb.width();

  std::vector<Vec3> stars = {};
  auto spawn_new_star = [&]()
  {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_real_distribution<> dis(-1.0, 1.0);
    float x = dis(gen)*factor;
    float y = dis(gen)*factor;

    if(dis(gen)<0.0)
    {
      if(x<0.0)
      {
        x -= fb.width();
      }
      else
      {
        x += fb.width();
      }
    }
    else
    {
      if(y<0.0)
      {
        y -= fb.height();
      }
      else
      {
        y += fb.height();
      }
    }
    stars.push_back({x, y, start_pos_y});
  };
  auto last = std::chrono::high_resolution_clock::now();
  float spawn_timer = 0.0;
  bool star_ready = false;
  for(int i = 0;;++i)
  {

    auto now = std::chrono::high_resolution_clock::now();
    float delta_seconds = ((float)std::chrono::duration_cast<std::chrono::milliseconds>(now - last).count())/1000.0;
    last = now;

    spawn_timer += delta_seconds;
    if(spawn_timer >= spawn_interval)
    {
      spawn_timer = 0.0;
      spawn_new_star();
    }

    for(size_t i = 0; i<stars.size(); ++i)
    {
      auto& star = stars[i];
      star.x += velocity.x*delta_seconds;
      star.y += velocity.y*delta_seconds;
      star.z += velocity.z*delta_seconds;

      if(star.z <= 0.0)
      {
        star_ready = true;
        stars.erase(stars.begin() + i);
        --i;
      }

    }
    if(star_ready)
    {
      fb.clear({0,0,0}, {255,255,255}, ' ');
      for(const auto& star : stars)
      {
        auto vec = perspective(star, vertical_field_of_view, horizontal_field_of_view);
        if(
          std::abs(vec.x)>=1.0 ||
          std::abs(vec.y)>=1.0 ||
          vec.z <= 0.0
        )
        {
          continue;
        }
        fb.setChar(
          vec.x*(fb.width()/2) + fb.width()/2,
          -vec.y*(fb.height()/2) + fb.height()/2,
          star_char
        );
      }
    }
    else
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(250));
      static int i = 0;
      i++;
      fb.clear({5, 100, 10}, {255,255,255}, ' ');
      fb.setChar(
        fb.width()/2 - knight[0].size()/2,
        fb.height()/2 - knight.size()/2,
        knight);
      std::vector<std::string> loading[4]
      {
        {"loading"},
        {"loading."},
        {"loading.."},
        {"loading..."}
      };
      constexpr Color l = Color{255,100,100};
      std::vector<std::vector<Color>> loading_text_color = {{l,l,l,l,l,l,l,l,l,l}};
      fb.setTextColor(
        fb.width()/2 - loading[3][0].size()/2,
        fb.height()/2 + knight.size()/2 + 1,
        loading_text_color
      );
      fb.setChar(
        fb.width()/2 - loading[3][0].size()/2,
        fb.height()/2 + knight.size()/2 + 1,
        loading[i%4]
      );
    }
    fb.print();
  }
  return 0;
}
