CC	= clang++
CFLAGS  = -std=c++17 -O3 -Wall -Wextra -Wpedantic -fno-omit-frame-pointer -g #-fsanitize=undefined
LDFLAGS = -std=c++17 -O3 -Wall -Wextra -Wpedantic -pthread -fno-omit-frame-pointer -g #-fsanitize=undefined
NAME = framebuffer
SRC_FILE_PATH = ./
BIN_FILE_PATH = ./build/

CPP = main.cpp

OBJ = $(CPP:%.cpp=$(BIN_FILE_PATH)%.o)

-include $(OBJ:%.o=%.d)

all: $(OBJ)
	$(CC) -o $(BIN_FILE_PATH)$(NAME) $(OBJ) $(LDFLAGS)

$(BIN_FILE_PATH)%.o: $(SRC_FILE_PATH)%.cpp
	$(CC) $(CFLAGS) -c $(SRC_FILE_PATH)$*.cpp -o $(BIN_FILE_PATH)$*.o
	echo -n $(BIN_FILE_PATH) > $(BIN_FILE_PATH)$*.d
	$(CC) $(CFLAGS) -MM $(SRC_FILE_PATH)$*.cpp >> $(BIN_FILE_PATH)$*.d

test: all
	$(BIN_FILE_PATH)$(NAME)

clean:
	rm $(BIN_FILE_PATH)*.o $(BIN_FILE_PATH)*.d $(BIN_FILE_PATH)$(NAME)
